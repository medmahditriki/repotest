﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DC
{
    public class FidelityFile : IFidelityFile
    {

        private FileInfo _fileInfo;

        public FidelityFile(FileInfo fileInfo)
        {
            _fileInfo = fileInfo;
        }

        public IList<IFidelityFileLine> List()
        {
            IList<IFidelityFileLine> list = new List<IFidelityFileLine>();
            using (StreamReader reader = _fileInfo.OpenText())
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    list.Add(new FidelityFileLine(line));
                }
            }

            return list;
        }
    }


    public class FidelityMemoryStream : IFidelityFile
    {
        private MemoryStream _stream;

        public FidelityMemoryStream(MemoryStream stream)
        {
            _stream = stream;
        }

        public IList<IFidelityFileLine> List()
        {
            IList<IFidelityFileLine> list = new List<IFidelityFileLine>();

            string text = System.Text.Encoding.UTF8.GetString(_stream.ToArray());

            string[] array = text.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string line in array)
            {
                list.Add(new FidelityFileLine(line));
            }

            return list;
        }
    }

    public class AccountFilteredFidelityFile : IFidelityFile
    {
        IFidelityFile _origin;
        string _accountNo;

        public AccountFilteredFidelityFile(IFidelityFile origin, string accountNo)
        {
            _origin = origin;
            _accountNo = accountNo;
        }

        public IList<IFidelityFileLine> List()
        {


            return _origin.List().Where(k => k.AccountNo() == _accountNo).ToList();
        }
    }
}
