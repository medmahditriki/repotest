﻿using System.Collections.Generic;

namespace DC
{
    public interface IFidelityFile
    {
        IList<IFidelityFileLine> List();
    }
}
