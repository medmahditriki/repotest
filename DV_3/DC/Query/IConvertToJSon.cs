﻿namespace DC.Query
{
    public interface IConvertToJSon
    {
        string ConvertingToJson(object obj);
    }
}
