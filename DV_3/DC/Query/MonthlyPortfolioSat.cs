﻿using System;
using System.Globalization;

namespace DC.Query
{
    public class MonthlyPortfolioSat : IMonthlyPortfolioSat
    {
        public double MonthEndTradeDateBalance;
        public double MonthEndSettlementDateBalance;
        public DateTime LoadDate;
        public string RecordSrc;
        public static string satellite_type = "monthly";

        public MonthlyPortfolioSat(FidelityFileLine line)
        {
            MonthEndTradeDateBalance = line.MonthEndTradeDateBalance();
            MonthEndSettlementDateBalance = line.MonthEndSettlementDateBalance();
            LoadDate = DateTime.ParseExact(System.DateTime.Now.ToString("yyyyMMdd"), "yyyyMMdd", CultureInfo.InvariantCulture);
            RecordSrc = "DC";
        }
    }
}
