﻿using System;

namespace DC.Query
{
    public interface IPortfolioHub
    {
        DateTime getLoadDate();
        string getRecordSrc();
        string getAccountNo(FidelityFileLine line);
    }
}
