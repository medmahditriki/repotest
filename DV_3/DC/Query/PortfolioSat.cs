﻿using System;
using System.Globalization;

namespace DC.Query
{
    public class PortfolioSat : IPortfolioSat
    {
        public string GuarantorAccount;
        public string GuarantorCode;
        public string AccountType;
        public string CurrencyCode1;
        public string IaNo;
        public string AccountClassCode;
        public string OptionCode;
        public string DebitInterestCode;
        public string CreditInterestCode;
        public string ResidenceCode;
        public string NRTCode;
        public string IAComment;
        public string UserComment;
        public string DeliveryCode;
        public DateTime AccountCloseDate;
        public double MarketValueforShortPositions;
        public string PortfolioFlag;
        public double AccountAccruedInterest1;
        public DateTime LastTradeDate1;
        public string AccountSubType1;
        public DateTime LastUpdateDateofAccountRecord;
        public string TradeOk;
        public string PortfolioType;
        public double AccountAccruedInterest2;
        public DateTime LastTradeDate2;
        public string AccountITF;
        public string RecordSrc;
        public DateTime LoadDate;
        public static string satellite_type = "standard";



        public PortfolioSat(FidelityFileLine line)
        {
            GuarantorCode = line.GuarantorCode();
            GuarantorAccount = line.GuarantorAccount();
            AccountType = line.AccountType();
            CurrencyCode1 = line.CurrencyCode1();
            IaNo = line.IaNo();
            AccountClassCode = line.AccountClassCode();
            OptionCode = line.OptionCode();
            DebitInterestCode = line.DebitInterestCode();
            CreditInterestCode = line.CreditInterestCode();
            NRTCode = line.NRTCode();
            IAComment = line.IAComment();
            UserComment = line.UserComment();
            DeliveryCode = line.DeliveryCode();
            AccountCloseDate = line.AccountCloseDate();
            MarketValueforShortPositions = line.MarketValueforShortPositions();
            PortfolioFlag = line.PortfolioFlag();
            AccountAccruedInterest1 = line.AccountAccruedInterest1();
            LastTradeDate1 = line.LastTradeDate1();
            AccountSubType1 = line.AccountSubType1();
            LastUpdateDateofAccountRecord = line.LastUpdateDateofAccountRecord();
            TradeOk = line.TradeOk();
            PortfolioType = line.PortfolioType();
            AccountAccruedInterest2 = line.AccountAccruedInterest2();
            LastTradeDate2 = line.LastTradeDate2();
            AccountITF = line.AccountITF();
            RecordSrc = "DC";
            LoadDate = DateTime.ParseExact(System.DateTime.Now.ToString("yyyyMMdd"), "yyyyMMdd", CultureInfo.InvariantCulture);

        }
    }
}
