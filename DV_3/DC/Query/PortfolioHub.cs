﻿using System;
using System.Globalization;

namespace DC.Query
{
    public class PortfolioHub : IPortfolioHub
    {
        public string AccountNo;
        public string RecordSrc;
        public DateTime LoadDate;
        public string getAccountNo(FidelityFileLine line)
        {
            AccountNo = line.AccountNo();
            return AccountNo;
        }
        public DateTime getLoadDate()
        {
            return DateTime.ParseExact(DateTime.Now.ToString("yyyyMMdd hh:mm:ss"), "yyyyMMdd hh:mm:ss", CultureInfo.InvariantCulture);
        }
        public string getRecordSrc()
        {
            RecordSrc = "DC";
            return RecordSrc;
        }
    }
}
