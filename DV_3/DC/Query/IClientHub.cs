﻿using System;

namespace DC.Query
{
    public interface IClientHub
    {
        DateTime getLoadDate();
        string getRecordSrc();
        string getAccountRoot(FidelityFileLine line);
    }
}
