﻿using System;


namespace DC.Query
{
    public class ClientSat : IClientSat
    {
        public string ClientID { get; set; }
        public string ResidenceCode { get; set; }
        public string Language1 { get; set; }
        public string AdressLine1{ get; set; }
        public string AdressLine2{ get; set; }
        public string AdressLine3{ get; set; }
        public string AdressLine4{ get; set; }
        public string AdressLine5{ get; set; }
        public string AdressLine6{ get; set; }
        public string AdressLine7{ get; set; }
        public string AdressLine8{ get; set; }
        public string PostCode{ get; set; }
        public string HomePhoneNumber{ get; set; }
        public string WorkPhoneNumber{ get; set; }
        public string FaxNumber{ get; set; }
        public string SIN{ get; set; }
        public double AnnualIncome{ get; set; }
        public double NetWorth{ get; set; }
        public string MaritalStatus{ get; set; }
        public double NumberofDependants{ get; set; }
        public DateTime AccountOpenDate{ get; set; }
        public string ClientLastName{ get; set; }
        public double HouseholdID{ get; set; }
        public string HouseholdName{ get; set; }
        public string Objecting{ get; set; }
        public string Mail{ get; set; }
        public string Email{ get; set; }
        public string ElectronicConfirmsFlag{ get; set; }
        public string ElectronicStatementsFlag{ get; set; }
        public DateTime BirthDate{ get; set; }
        public string FirstEmail{ get; set; }
        public string SecondEmail{ get; set; }
        public string ThirdEmail{ get; set; }
        public string FourthEmail{ get; set; }
        public string Gender{ get; set; }
        public DateTime LastUpdateDateofClientRecord{ get; set; }
        public DateTime LastUpdateDateofAddressRecord{ get; set; }
        public double RiskCode{ get; set; }
        public string SpouseName{ get; set; }
        public double SpouseSIN{ get; set; }
        public double ClientRecipientType{ get; set; }
        public string LastHousehold{ get; set; }
        public DateTime SpousalBirthDate{ get; set; }
        public double TimeHorizonYear{ get; set; }
        public double TimeHorizonCode{ get; set; }
        public double ClientNetWorth{ get; set; }
        public string Occupation{ get; set; }
        public string EmployerName{ get; set; }
        public string ShortName{ get; set; }
        public string ClientFirstName{ get; set; }

        public ClientSat(FidelityFileLine line)

        {
            ClientID = line.ClientID();
            ResidenceCode = line.ResidenceCode();
            ShortName = line.ShortName();
            Language1 = line.Language1();
            AdressLine1 = line.AdressLine1();
            AdressLine2 = line.AdressLine2();
            AdressLine3 = line.AdressLine3();
            AdressLine4 = line.AdressLine4();
            AdressLine5 = line.AdressLine5();
            AdressLine6 = line.AdressLine6();
            AdressLine7 = line.AdressLine7();
            AdressLine8 = line.AdressLine8();
            PostCode = line.PostCode();
            HomePhoneNumber = line.HomePhoneNumber();
            WorkPhoneNumber = line.WorkPhoneNumber();
            FaxNumber = line.FaxNumber();
            SIN = line.SIN();
            AnnualIncome = line.AnnualIncome();
            NetWorth = line.NetWorth();
            MaritalStatus = line.MaritalStatus();
            NumberofDependants = line.NumberofDependants();
            AccountOpenDate = line.AccountOpenDate();
            ClientLastName = line.ClientLastName();
            ClientFirstName = line.ClientFirstName();
            HouseholdID = line.HouseholdID();
            HouseholdName = line.HouseholdName();
            Objecting = line.Objecting();
            Mail = line.Mail();
            Email = line.Email();
            ElectronicConfirmsFlag = line.ElectronicConfirmsFlag();
            ElectronicStatementsFlag = line.ElectronicStatementsFlag();
            BirthDate = line.BirthDate();
            FirstEmail = line.FirstEmail();
            SecondEmail = line.SecondEmail();
            ThirdEmail = line.ThirdEmail();
            FourthEmail = line.FourthEmail();
            Gender = line.Gender();
            LastUpdateDateofClientRecord = line.LastUpdateDateofClientRecord();
            LastUpdateDateofAddressRecord = line.LastUpdateDateofAddressRecord();
            SpouseName = line.SpouseName();
            SpouseSIN = line.SpouseSIN();
            ClientRecipientType = line.ClientRecipientType();
            LastHousehold = line.LastHousehold();
            SpousalBirthDate = line.SpousalBirthDate();
            TimeHorizonYear = line.TimeHorizonYear();
            TimeHorizonCode = line.TimeHorizonCode();
            ClientNetWorth = line.ClientNetWorth();
            Occupation = line.Occupation();
            EmployerName = line.EmployerName();

        }

      
    }
}
