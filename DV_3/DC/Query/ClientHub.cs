﻿using System;
using System.Globalization;

namespace DC.Query
{
    public class ClientHub : IClientHub
    {
        public DateTime LoadDate { get; set; }
        public string RecordSrc { get; set; }
        public string AccountRoot { get; set; }
        public string getAccountRoot(FidelityFileLine line)
        {
            
            AccountRoot = line.AccountNo().Substring(0, line.AccountNo().Length - 1);
            return AccountRoot;
        }
        public DateTime getLoadDate()
        {
            return DateTime.ParseExact(System.DateTime.Now.ToString("yyyyMMdd hh:mm:ss"), "yyyyMMdd hh:mm:ss", CultureInfo.InvariantCulture);
        }
        public string getRecordSrc()
        {
            RecordSrc = "DC";
            return RecordSrc;
        }
    }
}
