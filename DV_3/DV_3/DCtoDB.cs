﻿using DC;
using DC.Query;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;

namespace DV_3
{
    public interface IDCtoDB
    {
        void SendDCtoDB(IFidelityFile file, string FileName);
    }
    public class DCtoDB : IDCtoDB
    {
        public void SendDCtoDB(IFidelityFile file, string FileName)
        {



            List<HUB_CLIENT> ListClientHub = new List<HUB_CLIENT>();
            List<HUB_PORTFOLIO> ListPortfolioHub = new List<HUB_PORTFOLIO>();

            Database.SetInitializer<data_vault_dev_db_01Entities>(new CreateDatabaseIfNotExists<data_vault_dev_db_01Entities>());
            using (var ctx = new data_vault_dev_db_01Entities())
            {
                List<HUB_PORTFOLIO> ExistingPfHubs = ctx.HUB_PORTFOLIO.Select(m => m).Distinct().ToList();
                List<HUB_CLIENT> ExistingClHubs = ctx.HUB_CLIENT.Select(m => m).Distinct().ToList();
                List<string> ExistingPfCodeHubs = ctx.HUB_PORTFOLIO.Select(m => m.CODE).Distinct().ToList();
                List<string> ExistingClCodeHubs = ctx.HUB_CLIENT.Select(m => m.CODE).Distinct().ToList();

                List<SAT_CLIENT> ExistingClSats = ctx.SAT_CLIENT.Select(m => m).Distinct().ToList();
                List<SAT_PORTFOLIO> ExistingDailySats = ctx.SAT_PORTFOLIO.Where(sat => sat.SATELLITE_TYPE == "DAILY").Distinct().ToList();
                List<SAT_PORTFOLIO> ExistingStandardSats = ctx.SAT_PORTFOLIO.Where(sat => sat.SATELLITE_TYPE == "STANDARD").Distinct().ToList();
                List<SAT_PORTFOLIO> ExistingMonthlySats = ctx.SAT_PORTFOLIO.Where(sat => sat.SATELLITE_TYPE == "MONTHLY").Distinct().ToList();


                SAT_PORTFOLIO PreviousPORTFOLIOSat = new SAT_PORTFOLIO();
                SAT_PORTFOLIO PreviousDAILYPORTFOLIOSat = new SAT_PORTFOLIO();
                SAT_PORTFOLIO PreviousMONTHLYPORTFOLIOSat = new SAT_PORTFOLIO();
                SAT_CLIENT PreviousCLIENTSat = new SAT_CLIENT();

                //int satPFcount=0, satCLcount=0;

                foreach (FidelityFileLine line in file.List())
                {
                    #region Preparing the RECORD

                    IClientSat __ClientSat = new ClientSat(line);
                    IClientHub __ClientHub = new ClientHub();
                    IPortfolioSat __PortfolioSat = new PortfolioSat(line);
                    IMonthlyPortfolioSat __MonthlyPortfolioSat = new MonthlyPortfolioSat(line);
                    IDailyPortfolioSat __DailyPortfolioSat = new DailyPortfolioSat(line);
                    IPortfolioHub __PortfolioHub = new PortfolioHub();

                    IConvertToJSon __ConvertToJson = new ConvertToJson();
                    string __JsonClientSat = __ConvertToJson.ConvertingToJson(__ClientSat);
                    DateTime Record_date_clientsat = new DateTime(Int32.Parse(FileName.Substring(2, 2)) + 2000, Int32.Parse(FileName.Substring(4, 2)), Int32.Parse(FileName.Substring(6, 2)));

                    string __JsonPortfolioSat = __ConvertToJson.ConvertingToJson(__PortfolioSat);
                    DateTime Record_date_standard = new DateTime(Int32.Parse(FileName.Substring(2, 2)) + 2000, Int32.Parse(FileName.Substring(4, 2)), Int32.Parse(FileName.Substring(6, 2)));

                    string __JsonMonthlyPortfolioSat = __ConvertToJson.ConvertingToJson(__MonthlyPortfolioSat);
                    DateTime Record_date_monthly = new DateTime(Int32.Parse(FileName.Substring(2, 2)) + 2000, Int32.Parse(FileName.Substring(4, 2)), Int32.Parse(FileName.Substring(6, 2)));

                    string __JsonDailyPortfolioSat = __ConvertToJson.ConvertingToJson(__DailyPortfolioSat);
                    DateTime Record_date_daily = new DateTime(Int32.Parse(FileName.Substring(2, 2)) + 2000, Int32.Parse(FileName.Substring(4, 2)), Int32.Parse(FileName.Substring(6, 2)));

                    #endregion
                    #region Insert Client_Hub in DB

                    HUB_CLIENT clienthub = new HUB_CLIENT();
                    clienthub.CODE = __ClientHub.getAccountRoot(line);
                    clienthub.LOAD_DATE = __ClientHub.getLoadDate();
                    clienthub.RECORD_SRC = __ClientHub.getRecordSrc();

                    bool New_Hub = !ExistingClCodeHubs.Contains(clienthub.CODE);
                    List<string> NewClHubsCodes = ListClientHub.Select(hub => hub.CODE).ToList();
                    if (New_Hub)
                    {
                        ExistingClHubs.Add(clienthub);
                        ExistingClCodeHubs.Add(clienthub.CODE);
                        //ExistingClCodeHubs = ExistingClCodeHubs.Distinct().ToList();
                        ListClientHub.Add(clienthub);
                        NewClHubsCodes.Add(clienthub.CODE);
                        //ListClientHub = ListClientHub.GroupBy(hub => hub.CODE).Select(hub => hub.OrderByDescending(h => h.LOAD_DATE).First()).ToList();
                    }





                    #endregion

                    #region Insert Portfolio_Hub in DB


                    HUB_PORTFOLIO portfoliohub = new HUB_PORTFOLIO();
                    portfoliohub.LOAD_DATE = __PortfolioHub.getLoadDate();
                    portfoliohub.RECORD_SRC = __PortfolioHub.getRecordSrc();
                    portfoliohub.CODE = __PortfolioHub.getAccountNo(line);

                    bool New_Hub2 = !ExistingPfCodeHubs.Contains(portfoliohub.CODE);
                    List<string> NewPfHubsCodes = ListPortfolioHub.Select(hub => hub.CODE).ToList();
                    if (New_Hub2)
                    {
                        ExistingPfCodeHubs.Add(portfoliohub.CODE);
                        //ExistingPfCodeHubs = ExistingPfCodeHubs.Distinct().ToList();
                        ExistingPfHubs.Add(portfoliohub);
                        ListPortfolioHub.Add(portfoliohub);
                        NewPfHubsCodes.Add(portfoliohub.CODE);
                        //ListPortfolioHub = ListPortfolioHub.GroupBy(hub => hub.CODE).Select(hub => hub.OrderByDescending(h => h.LOAD_DATE).FirstOrDefault()).ToList();
                    }


                    #endregion
                    #region Insert Portfolio_Sat in DB

                    SAT_PORTFOLIO portfoliosat_monthly = new SAT_PORTFOLIO();
                    SAT_PORTFOLIO portfoliosat = new SAT_PORTFOLIO();
                    SAT_PORTFOLIO portfoliosat_daily = new SAT_PORTFOLIO();

                    portfoliosat_daily.RECORD = __JsonDailyPortfolioSat;
                    portfoliosat_daily.RECORD_DATE = Record_date_daily;
                    portfoliosat_daily.SATELLITE_TYPE = "DAILY";
                    portfoliosat_daily.RECORD_SRC = "DC";
                    portfoliosat_daily.LOAD_DATE = DateTime.ParseExact(System.DateTime.Now.ToString("yyyyMMdd hh:mm:ss"), "yyyyMMdd hh:mm:ss", CultureInfo.InvariantCulture);


                    portfoliosat_monthly.RECORD = __JsonMonthlyPortfolioSat;
                    portfoliosat_monthly.RECORD_DATE = Record_date_monthly;
                    portfoliosat_monthly.SATELLITE_TYPE = "MONTHLY";
                    portfoliosat_monthly.RECORD_SRC = "DC";
                    portfoliosat_monthly.LOAD_DATE = DateTime.ParseExact(System.DateTime.Now.ToString("yyyyMMdd hh:mm:ss"), "yyyyMMdd hh:mm:ss", CultureInfo.InvariantCulture);


                    portfoliosat.RECORD = __JsonPortfolioSat;
                    portfoliosat.RECORD_DATE = Record_date_standard;
                    portfoliosat.SATELLITE_TYPE = "STANDARD";
                    portfoliosat.RECORD_SRC = "DC";
                    portfoliosat.LOAD_DATE = DateTime.ParseExact(System.DateTime.Now.ToString("yyyyMMdd hh:mm:ss"), "yyyyMMdd hh:mm:ss", CultureInfo.InvariantCulture);





                    if ((New_Hub2))
                    {
                        ListPortfolioHub.Where(hub => hub.CODE == portfoliohub.CODE).First().SAT_PORTFOLIO.Add(portfoliosat);
                        ListPortfolioHub.Where(hub => hub.CODE == portfoliohub.CODE).First().SAT_PORTFOLIO.Add(portfoliosat_daily);
                        ListPortfolioHub.Where(hub => hub.CODE == portfoliohub.CODE).First().SAT_PORTFOLIO.Add(portfoliosat_monthly);


                    }
                    else
                    {
                        long Actual_SQN = ExistingPfHubs.Where(hub => hub.CODE == portfoliohub.CODE).First().SQN;
                        PreviousMONTHLYPORTFOLIOSat = ExistingMonthlySats.Where(sat => sat.SQN == Actual_SQN && sat.SATELLITE_TYPE == "MONTHLY").OrderByDescending(sat => sat.LOAD_DATE).FirstOrDefault();
                        PreviousPORTFOLIOSat = ExistingStandardSats.Where(sat => sat.SQN == Actual_SQN && sat.SATELLITE_TYPE == "STANDARD").OrderByDescending(sat => sat.LOAD_DATE).FirstOrDefault();
                        PreviousDAILYPORTFOLIOSat = ExistingDailySats.Where(sat => sat.SQN == Actual_SQN && sat.SATELLITE_TYPE == "DAILY").OrderByDescending(sat => sat.LOAD_DATE).FirstOrDefault();
                        if (PreviousDAILYPORTFOLIOSat != null)
                        {
                            if ((PreviousDAILYPORTFOLIOSat.RECORD != portfoliosat_daily.RECORD) && (PreviousDAILYPORTFOLIOSat.RECORD_DATE < portfoliosat_daily.RECORD_DATE))
                            {
                                if (NewPfHubsCodes.Contains(portfoliohub.CODE))
                                {
                                    ListPortfolioHub.Where(hub => hub.CODE == portfoliohub.CODE).First().SAT_PORTFOLIO.Add(portfoliosat_daily);
                                    //satPFcount++ ;
                                }
                                else
                                {
                                    HUB_PORTFOLIO Actual_Hub = ExistingPfHubs.Where(hub => hub.CODE == portfoliohub.CODE).First();
                                    ctx.HUB_PORTFOLIO.Where(hub => hub.CODE == Actual_Hub.CODE).First().SAT_PORTFOLIO.Add(portfoliosat_daily);
                                    //satPFcount++;//
                                    ctx.SaveChanges();
                                }
                            }
                        }
                        if (PreviousPORTFOLIOSat != null)
                        {
                            if ((PreviousPORTFOLIOSat.RECORD != portfoliosat.RECORD) && (PreviousPORTFOLIOSat.RECORD_DATE < portfoliosat.RECORD_DATE))
                            {
                                if (NewPfHubsCodes.Contains(portfoliohub.CODE))
                                {
                                    ListPortfolioHub.Where(hub => hub.CODE == portfoliohub.CODE).First().SAT_PORTFOLIO.Add(portfoliosat);
                                    //satPFcount++;
                                }
                                else
                                {
                                    HUB_PORTFOLIO Actual_Hub = ExistingPfHubs.Where(hub => hub.CODE == portfoliohub.CODE).First();
                                    ctx.HUB_PORTFOLIO.Where(hub => hub.CODE == Actual_Hub.CODE).First().SAT_PORTFOLIO.Add(portfoliosat);
                                    //satPFcount++;
                                    ctx.SaveChanges();
                                }

                            }
                        }
                        if (PreviousMONTHLYPORTFOLIOSat != null)
                        {
                            if ((PreviousMONTHLYPORTFOLIOSat.RECORD_DATE < portfoliosat_monthly.RECORD_DATE) && (PreviousMONTHLYPORTFOLIOSat.RECORD != portfoliosat_monthly.RECORD))
                            {
                                if (NewPfHubsCodes.Contains(portfoliohub.CODE))
                                {
                                    ListPortfolioHub.Where(hub => hub.CODE == portfoliohub.CODE).First().SAT_PORTFOLIO.Add(portfoliosat_monthly);
                                    //satPFcount++;
                                }
                                else
                                {
                                    HUB_PORTFOLIO Actual_Hub = ExistingPfHubs.Where(hub => hub.CODE == portfoliohub.CODE).First();
                                    ctx.HUB_PORTFOLIO.Where(hub => hub.CODE == Actual_Hub.CODE).First().SAT_PORTFOLIO.Add(portfoliosat_monthly);
                                    //satPFcount++;
                                    ctx.SaveChanges();
                                }
                            }
                        }
                        else
                        {
                            if (ListPortfolioHub.Where(hub => hub.CODE == portfoliohub.CODE).Count() != 0)
                            {
                                ListPortfolioHub.Where(hub => hub.CODE == portfoliohub.CODE).First().SAT_PORTFOLIO.Add(portfoliosat);
                                ListPortfolioHub.Where(hub => hub.CODE == portfoliohub.CODE).First().SAT_PORTFOLIO.Add(portfoliosat_daily);
                                ListPortfolioHub.Where(hub => hub.CODE == portfoliohub.CODE).First().SAT_PORTFOLIO.Add(portfoliosat_monthly);
                                //satPFcount=+3;
                            }

                        }
                    }

                    #endregion

                    #region Insert Client_Sat in DB

                    SAT_CLIENT clientsat = new SAT_CLIENT();
                    clientsat.RECORD = __JsonClientSat;
                    clientsat.LOAD_DATE = DateTime.ParseExact(System.DateTime.Now.ToString("yyyyMMdd hh:mm:ss"), "yyyyMMdd hh:mm:ss", CultureInfo.InvariantCulture);
                    clientsat.SATELLITE_TYPE = "CLIENT";
                    clientsat.RECORD_DATE = Record_date_clientsat;
                    clientsat.RECORD_SRC = "DC";





                    if (New_Hub)
                    {
                        ListClientHub.Where(hub => hub.CODE == clienthub.CODE).First().SAT_CLIENT.Add(clientsat);

                    }
                    else
                    {
                        long Actual_SQN = ExistingClHubs.Where(hub => hub.CODE == clienthub.CODE).First().SQN;
                        PreviousCLIENTSat = ExistingClSats.Where(sat => sat.SQN == Actual_SQN).OrderByDescending(sat => sat.LOAD_DATE).FirstOrDefault();
                        if (PreviousCLIENTSat != null)

                        {
                            if ((PreviousCLIENTSat.RECORD != clientsat.RECORD && PreviousCLIENTSat.RECORD_DATE < clientsat.RECORD_DATE))
                            {
                                if (NewClHubsCodes.Contains(clienthub.CODE))
                                {
                                    ListClientHub.Where(hub => hub.CODE == clienthub.CODE).First().SAT_CLIENT.Add(clientsat);
                                    //satCLcount++;
                                }
                                else
                                {
                                    //HUB_CLIENT Actual_Hub = ExistingClHubs.Where(hub => hub.CODE == clienthub.CODE).First();
                                    ctx.HUB_CLIENT.Where(hub => hub.SQN == Actual_SQN).First().SAT_CLIENT.Add(clientsat);
                                    //satCLcount++;
                                    ctx.SaveChanges();
                                }
                            }
                        }
                        // else
                        // {
                        //     if (ListClientHub.Where(hub => hub.CODE == clienthub.CODE).Count() != 0 && !NewClHubsCodes.Contains(clienthub.CODE))
                        //     {
                        //         ListClientHub.Where(hub => hub.CODE == clienthub.CODE).First().SAT_CLIENT.Add(clientsat);
                        //     }
                        // }
                        else
                        {
                            if (ListClientHub.Where(hub => hub.CODE == clienthub.CODE).Count() != 0)
                            {
                                if (ListClientHub.Where(hub => hub.CODE == clienthub.CODE).FirstOrDefault().SAT_CLIENT.Count() == 0)
                                {
                                    ListClientHub.Where(hub => hub.CODE == clienthub.CODE).First().SAT_CLIENT.Add(clientsat);
                                    //satCLcount++;
                                }
                                else
                                {
                                    SAT_CLIENT PreviousSatInUninsertedHub = ListClientHub.Where(hub => hub.CODE == clienthub.CODE).FirstOrDefault().SAT_CLIENT.OrderBy(sat => sat.LOAD_DATE).FirstOrDefault();
                                    if (PreviousSatInUninsertedHub != null)
                                    {
                                        if ((PreviousSatInUninsertedHub.RECORD != clientsat.RECORD && PreviousSatInUninsertedHub.RECORD_DATE < clientsat.RECORD_DATE))
                                        {
                                            ListClientHub.Where(hub => hub.CODE == clienthub.CODE).First().SAT_CLIENT.Add(clientsat);
                                            //satCLcount++;

                                        }
                                    }
                                }
                            }
                        }



                    }
                    if (!ExistingClSats.Contains(clientsat))
                    {
                        ExistingClSats.Add(clientsat);
                    }

                    #endregion


                }

                #region Save into DB



                ctx.Set<HUB_CLIENT>().AddRange(ListClientHub);
                ctx.Set<HUB_PORTFOLIO>().AddRange(ListPortfolioHub);







                ctx.SaveChanges();
                //Console.WriteLine("satCLcount = " + satCLcount);
                //Console.WriteLine("satPFcount = " + satPFcount);
                //Console.ReadKey();
                #endregion
            }

        }
    }
}
