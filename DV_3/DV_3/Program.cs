﻿using DC;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;

namespace DV_3
{
    class Program
    {

        static void Main(string[] args)
        {
            #region Connecting to Blob
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference("dataphile-dc");
            container.CreateIfNotExists();
            // lire le fichier du blob
            string FileName = "dc141231.all.JIT";
            CloudBlockBlob blockBlob = container.GetBlockBlobReference("dc141231.all.JIT");
            //  CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName);

            MemoryStream memoryStream;
            using (memoryStream = new MemoryStream())

            { blockBlob.DownloadToStream(memoryStream); }
            IFidelityFile file = new FidelityMemoryStream(memoryStream);

            #endregion
            #region Inserting DC data into DB

            IDCtoDB DCtoDB = new DCtoDB();
            DCtoDB.SendDCtoDB(file, FileName);

            #endregion
        }

    }

}


