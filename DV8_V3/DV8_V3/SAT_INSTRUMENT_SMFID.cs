﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DV8_V3
{
    public class SAT_INSTRUMENT_SMFID
    {
        public long SQN;
        
        public string Json;
        public string SecurityType;
        public string UnderlyingCode;
        public string OptionType;
        /// <summary>
        /// gets all SMFID from SAT_INSTRUMENT
        /// </summary>
        /// <param name="HubList"></param>
        /// <returns></returns>
        public List<SAT_INSTRUMENT_SMFID> getSAT_SMFID(List<HUB_INSTRUMENT> HubList, List<long> SQNList)
        {
            List<SAT_INSTRUMENT_SMFID> SMFID_List = new List<SAT_INSTRUMENT_SMFID>();
            string ConnectionString_DV =
                           "Data Source= jitney-dev-db-01.database.windows.net;" +
                           "Initial Catalog=data_vault_dev_db_01;" +
                           "User id=JitneyDev;" +
                           "Password=J!tneyd3v99;";
            using (SqlConnection connection = new SqlConnection(ConnectionString_DV))
            {

                string sql_DV_SMFID = "select t.SQN,t.RECORD " +
                                   " from SAT_INSTRUMENT t " +
                                   " INNER JOIN( " +
                                   " select SQN,Max(LOAD_DATE) as Max_LOAD_DATE " +
                                   " from SAT_INSTRUMENT " +
                                   " where RECORD_SRC ='SMFID' " +
                                   " group by SQN ) tm on " +
                                   " t.SQN = tm.SQN AND t.LOAD_DATE = tm.Max_LOAD_DATE";
                connection.Open();
                using (SqlCommand command_DV = new SqlCommand(sql_DV_SMFID, connection))
                {
                    using (SqlDataReader dataread_DV = command_DV.ExecuteReader())
                    {
                        while (dataread_DV.Read())
                        {
                            SAT_INSTRUMENT_SMFID SMFID = new SAT_INSTRUMENT_SMFID();
                            SMFID.SQN = Convert.ToInt32(dataread_DV["SQN"]);
                            SMFID.Json = dataread_DV["RECORD"].ToString();
                            
                            var DataSMFID = (JObject)JsonConvert.DeserializeObject(SMFID.Json);
                            SMFID.SecurityType = DataSMFID["ClassCode"].Value<string>();
                            SMFID.UnderlyingCode = DataSMFID["UnderlyingCusip"].Value<string>();
                            SMFID.OptionType = DataSMFID["OptionCallPut"].Value<string>();
                            if(SQNList.Contains(SMFID.SQN))
                            SMFID_List.Add(SMFID);

                        }
                    }

                }
                connection.Close();
            }
            return SMFID_List;
        }
        /// <summary>
        /// get all SQNs of SM List
        /// </summary>
        /// <param name="SATSMList"></param>
        /// <returns></returns>
        //public List<long> getSQNList(List<SAT_INSTRUMENT_SMFID> SATSMFIDList)
        //{
        //    List<long> SQNs = new List<long>();
        //    SQNs = SATSMFIDList.Select(sat => sat.SQN).Distinct().ToList();
        //    return SQNs;
        //}
        //public List<string> getRECORDList(List<SAT_INSTRUMENT_SMFID> SATSMFIDList)
        //{
        //    List<string> RECORDs = new List<string>();
        //    RECORDs = SATSMFIDList.Select(sat => sat.Json).Distinct().ToList();
        //    return RECORDs;
        //}
    }
}
