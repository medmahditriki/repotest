﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DV8_V3
{
    public class HUB_INSTRUMENT
    {
        public long SQN;
        public string CODE;
        /// <summary>
        /// gets all Hubs from HUB_INSTRUMENT
        /// </summary>
        /// <returns></returns>
        public List<HUB_INSTRUMENT> getHubList()
        {
            List<HUB_INSTRUMENT> HubList = new List<HUB_INSTRUMENT>();
            string ConnectionString_DV =
                            "Data Source= jitney-dev-db-01.database.windows.net;" +
                            "Initial Catalog=data_vault_dev_db_01;" +
                            "User id=JitneyDev;" +
                            "Password=J!tneyd3v99;";
            using (SqlConnection connection = new SqlConnection(ConnectionString_DV))
            {

                string sql_DV_Hub = "select CODE,SQN from HUB_INSTRUMENT ";
                connection.Open();
                using (SqlCommand command_DV = new SqlCommand(sql_DV_Hub, connection))
                {
                    using (SqlDataReader dataread_DV = command_DV.ExecuteReader())
                    {
                        while (dataread_DV.Read())
                        {
                            HUB_INSTRUMENT HUB_INSTRUMENT = new HUB_INSTRUMENT();
                            HUB_INSTRUMENT.SQN = Convert.ToInt32(dataread_DV["SQN"]);
                            HUB_INSTRUMENT.CODE = dataread_DV["CODE"].ToString();

                            HubList.Add(HUB_INSTRUMENT);

                        }
                    }

                }
                connection.Close();
            }
                return HubList;
        }
        /// <summary>
        /// gets hubs that fits with CODEs got from DIM_INSTRUMENT
        /// </summary>
        /// <param name="NullSymbolList"></param>
        /// <param name="AllHubs"></param>
        /// <returns></returns>
        public  List<HUB_INSTRUMENT> getNullableHubList(List<string> NullSymbolList, List<HUB_INSTRUMENT> AllHubs)
        {
            List<HUB_INSTRUMENT> NullSymbolHubs = new List<HUB_INSTRUMENT>();
           
            NullSymbolHubs = AllHubs.Where(hub => NullSymbolList.Contains(hub.CODE)).ToList();
            return NullSymbolHubs;
        }
        /// <summary>
        /// Get New hubs which are not inserted in DIM_INSTRUMENT
        /// </summary>
        /// <param name="DIMList">Inserted columns</param>
        /// <param name="HUBList">Hubs from HUB_INSTRUMENT</param>
        /// <returns></returns>
        public List<HUB_INSTRUMENT> getDifference(List<D_INSTRUMENT> DIMList, List<HUB_INSTRUMENT> HUBList)
        {

            List<string> CodeDIMList = DIMList.Select(hub => hub.CODE).Distinct().ToList();
            List<HUB_INSTRUMENT> NewHubs = HUBList.Where(dim => !CodeDIMList.Contains(dim.CODE)).ToList();
            return NewHubs;
        }
        /// <summary>
        /// gets all SQNs of a HUB List
        /// </summary>
        /// <param name="HubInstrumentList"></param>
        /// <returns></returns>
        public List<long> getSQNList(List<HUB_INSTRUMENT> HubInstrumentList)
        {
            List<long> SQNs = new List<long>();
            SQNs = HubInstrumentList.Select(hub => hub.SQN).Distinct().ToList();
            return SQNs;
        }
        //public List<string> getCODEList(List<HUB_INSTRUMENT> HubInstrumentList)
        //{
        //    List<string> CODEs = new List<string>();
        //    CODEs = HubInstrumentList.Select(hub => hub.CODE).Distinct().ToList();
        //    return CODEs;
        //}
        //public List<HUB_INSTRUMENT> getNewHubs(List<HUB_INSTRUMENT> HubInstrumentList, List<string> DIMCODEList)
        //{
        //    List<HUB_INSTRUMENT> NewHubs = new List<HUB_INSTRUMENT>();
        //    NewHubs = HubInstrumentList.Where(hub => DIMCODEList.Contains(hub.CODE)).ToList();

        //    return NewHubs;
        //}
    }
}
