﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
namespace DV8_V3
{
    public class D_INSTRUMENT
    {
        public string CODE;
        public string SYMBOL;
        public HUB_INSTRUMENT HUB_INSTRUMENT;
        /// <summary>
        /// get all rows from DIM_INSTRUMENT
        /// </summary>
        /// <returns></returns>
        public List<D_INSTRUMENT> getDIMList()
        {
            List<D_INSTRUMENT> ListCode_Symbol = new List<D_INSTRUMENT>();
            string ConnectionString_DIM = "data source=(LocalDB)\\LocalDBTest;initial catalog=DIMENSION;integrated security=True;MultipleActiveResultSets=True;";
            using (SqlConnection connection = new SqlConnection(ConnectionString_DIM))
            {

                string sql_DIM = "select CODE,SYMBOL from ANOTHER_INSTRUMENT ";
                connection.Open();
                using (SqlCommand command_DIM = new SqlCommand(sql_DIM, connection))
                {
                    using (SqlDataReader dataread_DIM = command_DIM.ExecuteReader())
                    {
                        while (dataread_DIM.Read())
                        {
                            D_INSTRUMENT D_INSTRUMENT = new D_INSTRUMENT();
                            D_INSTRUMENT.CODE = dataread_DIM["CODE"].ToString();
                            D_INSTRUMENT.SYMBOL = dataread_DIM["SYMBOL"].ToString();

                            ListCode_Symbol.Add(D_INSTRUMENT);

                        }
                    }

                }
                connection.Close();

            }

            return ListCode_Symbol;
        }
        /// <summary>
        /// gets all rows with SYMBOL = NULL from DIM_INSTRUMENT
        /// </summary>
        /// <param name="DIMList">DIM_INSTRUMENT rows</param>
        /// <returns></returns>
        public List<string> getNullableSymbolCodeList(List<D_INSTRUMENT> DIMList)
        {
            List<string> CodeList = new List<string>();
            CodeList = DIMList.Where(dim => dim.SYMBOL.Replace(" ", "") == "").Select(dim => dim.CODE).Distinct().ToList();
            return CodeList;
        }
        /// <summary>
        /// Inserts hubs which are not inserted in DIM_INSTRUMENT
        /// </summary>
        /// <param name="SQNList">SQN of getdifference() hubs</param>
        /// <param name="HUBList">hubs to insert</param>
        /// <param name="SMList">SM for attributes</param>
        /// <param name="SMFIDList">SMFID for attributes</param>
        public void InsertNewHubs(List<long> SQNList, List<HUB_INSTRUMENT> HUBList, List<SAT_INSTRUMENT_SM> SMList, List<SAT_INSTRUMENT_SMFID> SMFIDList)
        {
            List<ANOTHER_INSTRUMENT> ANOTHER_INSTRUMENTList = new List<ANOTHER_INSTRUMENT>();

            foreach (long SQN in SQNList)
            {
                ANOTHER_INSTRUMENT ANOTHER_INSTRUMENT = new ANOTHER_INSTRUMENT();
                ANOTHER_INSTRUMENT.CODE = HUBList.Where(hub => hub.SQN == SQN).First().CODE;
                ANOTHER_INSTRUMENT.ID_TYPE = "CUSIP";
                SAT_INSTRUMENT_SM SM = SMList.Where(sm => sm.SQN == SQN).FirstOrDefault();
                SAT_INSTRUMENT_SMFID SMFID = SMFIDList.Where(sm2 => sm2.SQN == SQN).FirstOrDefault();
                if (SM != null)
                {

                    if (SM.Symbol != null && SM.Symbol.Replace(" ", "") != "")
                    {
                        ANOTHER_INSTRUMENT.SYMBOL = SM.Symbol;
                        ANOTHER_INSTRUMENT.CURRENCY = SM.Currency;

                        if (SMFID != null)
                        {
                            ANOTHER_INSTRUMENT.OPTION_TYPE = SMFID.OptionType;
                            ANOTHER_INSTRUMENT.SECURITY_TYPE = SMFID.SecurityType;
                            ANOTHER_INSTRUMENT.UNDERLYING_CODE = SMFID.UnderlyingCode;
                        }
                    }
                }
                ANOTHER_INSTRUMENTList.Add(ANOTHER_INSTRUMENT);
            }
            Database.SetInitializer<DIMENSIONEntities>(new CreateDatabaseIfNotExists<DIMENSIONEntities>());
            using (var ctx = new DIMENSIONEntities())
            {
                //ctx.Set<ANOTHER_INSTRUMENT>().AddRange(ANOTHER_INSTRUMENTList);
                //foreach (var dim in ANOTHER_INSTRUMENTList)
                //{
                //    if (dim.CODE == "000021303")
                //    {
                //        Console.WriteLine(dim.CODE);
                //    }

                     ctx.Set<ANOTHER_INSTRUMENT>().AddRange(ANOTHER_INSTRUMENTList);
                    ctx.SaveChanges(); 
                //}
                //Console.WriteLine("ok");
                //Console.ReadKey();
            }
        }
        /// <summary>
        /// update data when SYMBOL == NULL 
        /// </summary>
        /// <param name="SQNList"></param>
        /// <param name="HUBList"></param>
        /// <param name="SMList"></param>
        /// <param name="SMFIDList"></param>
        public void UpdateOldDims(List<long> SQNList, List<HUB_INSTRUMENT> HUBList, List<SAT_INSTRUMENT_SM> SMList, List<SAT_INSTRUMENT_SMFID> SMFIDList)
        {
            string ConnectionString_DIM = "data source=(LocalDB)\\LocalDBTest;initial catalog=DIMENSION;integrated security=True;MultipleActiveResultSets=True;";

            using (SqlConnection connection = new SqlConnection(ConnectionString_DIM))
            {

                string sql_DIM = "UPDATE ANOTHER_INSTRUMENT(CODE,ID_TYPE,SYMBOL,CURRENCY,SECURITY_TYPE,UNDERLYING_CUSIP,OPTION_TYPE) VALUES(@C,@I,@S,@CU,@ST,@UC,@OT) where SYMBOL= " + null;
                connection.Open();

                foreach (long SQN in SQNList)
                {
                    using (SqlCommand command_DIM = new SqlCommand(sql_DIM, connection))
                    {
                        command_DIM.Parameters.AddWithValue("@C", HUBList.Where(hub => hub.SQN == SQN).First().CODE);
                        command_DIM.Parameters.AddWithValue("@I", "CUSIP");
                        if (SMList.Where(sat => sat.SQN == SQN).FirstOrDefault() != null)
                            command_DIM.Parameters.AddWithValue("@S", SMList.Where(sat => sat.SQN == SQN).FirstOrDefault().Symbol);
                        if (SMList.Where(sat => sat.SQN == SQN).FirstOrDefault() != null)
                            command_DIM.Parameters.AddWithValue("@CU", SMList.Where(sat => sat.SQN == SQN).FirstOrDefault().Currency);
                        if (SMFIDList.Where(sat => sat.SQN == SQN).FirstOrDefault() != null)
                            command_DIM.Parameters.AddWithValue("@ST", SMFIDList.Where(sat => sat.SQN == SQN).FirstOrDefault().SecurityType);
                        if (SMFIDList.Where(sat => sat.SQN == SQN).FirstOrDefault() != null)
                            command_DIM.Parameters.AddWithValue("@UC", SMFIDList.Where(sat => sat.SQN == SQN).FirstOrDefault().UnderlyingCode);
                        if (SMFIDList.Where(sat => sat.SQN == SQN).FirstOrDefault() != null)
                            command_DIM.Parameters.AddWithValue("@OT", SMFIDList.Where(sat => sat.SQN == SQN).FirstOrDefault().OptionType);

                    }
                }
                connection.Close();
            }
        }
    }
}
