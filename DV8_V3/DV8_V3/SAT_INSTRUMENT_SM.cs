﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DV8_V3
{
    public class SAT_INSTRUMENT_SM
    {
        public long SQN;
        
        public string Json;
        public string Symbol;
        public string Currency;
        /// <summary>
        /// gets all SM from SAT_INSTRUMENT
        /// </summary>
        /// <param name="HubList"></param>
        /// <returns></returns>
        public List<SAT_INSTRUMENT_SM> getSAT_SM(List<HUB_INSTRUMENT> HubList)
        {
            List<SAT_INSTRUMENT_SM> SM_List = new List<SAT_INSTRUMENT_SM>();
            string ConnectionString_DV =
                           "Data Source= jitney-dev-db-01.database.windows.net;" +
                           "Initial Catalog=data_vault_dev_db_01;" +
                           "User id=JitneyDev;" +
                           "Password=J!tneyd3v99;";
            using (SqlConnection connection = new SqlConnection(ConnectionString_DV))
            {

                string sql_DV_SM = "select t.SQN,t.RECORD " +
                                   " from SAT_INSTRUMENT t " +
                                   " INNER JOIN( " +
                                   " select SQN,Max(LOAD_DATE) as Max_LOAD_DATE " +
                                   " from SAT_INSTRUMENT " +
                                   " where RECORD_SRC ='SM' " +
                                   " group by SQN ) tm on " +
                                   " t.SQN = tm.SQN AND t.LOAD_DATE = tm.Max_LOAD_DATE";
                connection.Open();
                using (SqlCommand command_DV = new SqlCommand(sql_DV_SM, connection))
                {
                    using (SqlDataReader dataread_DV = command_DV.ExecuteReader())
                    {
                        while (dataread_DV.Read())
                        {
                            SAT_INSTRUMENT_SM SM = new SAT_INSTRUMENT_SM();
                            SM.SQN = Convert.ToInt32(dataread_DV["SQN"]);
                            SM.Json = dataread_DV["RECORD"].ToString();
                            var DataSM = (JObject)JsonConvert.DeserializeObject(SM.Json);
                            SM.Currency = DataSM["CurrencyCode"].Value<string>();
                            SM.Symbol = DataSM["Symbol"].Value<string>();
                            if(SM.Symbol.Replace(" ","") != "" && SM.Symbol !=null)
                            SM_List.Add(SM);

                        }
                    }

                }
                connection.Close();
            }
            return SM_List;
        }
        /// <summary>
        /// get all SQNs of SM List
        /// </summary>
        /// <param name="SATSMList"></param>
        /// <returns></returns>
        public List<long> getSQNList(List<SAT_INSTRUMENT_SM> SATSMList)
        {
            List<long> SQNs = new List<long>();
            SQNs = SATSMList.Select(sat => sat.SQN).Distinct().ToList();
            return SQNs;
        }
        //public List<string> getRECORDList(List<SAT_INSTRUMENT_SM> SATSMList)
        //{
        //    List<string> RECORDs = new List<string>();
        //    RECORDs = SATSMList.Select(sat => sat.Json).Distinct().ToList();
        //    return RECORDs;
        //}
    }
}
