﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DV8_V3
{
    class Program
    {
        static void Main(string[] args)
        {
            D_INSTRUMENT D_INSTRUMENT = new D_INSTRUMENT();
            HUB_INSTRUMENT HUB_INSTRUMENT = new HUB_INSTRUMENT();
            SAT_INSTRUMENT_SM SAT_INSTRUMENT_SM = new SAT_INSTRUMENT_SM();
            SAT_INSTRUMENT_SMFID SAT_INSTRUMENT_SMFID = new SAT_INSTRUMENT_SMFID();

            #region Insert New Dims
            List<D_INSTRUMENT> D_INSTRUMENT_List = D_INSTRUMENT.getDIMList();
            List<HUB_INSTRUMENT> HUB_INSTRUMENTList = HUB_INSTRUMENT.getHubList();
            List<HUB_INSTRUMENT> NewDims = HUB_INSTRUMENT.getDifference(D_INSTRUMENT_List, HUB_INSTRUMENTList);
            List<SAT_INSTRUMENT_SM> ListSM = SAT_INSTRUMENT_SM.getSAT_SM(HUB_INSTRUMENTList);
            List<SAT_INSTRUMENT_SMFID> ListSMFID = SAT_INSTRUMENT_SMFID.getSAT_SMFID(HUB_INSTRUMENTList,SAT_INSTRUMENT_SM.getSQNList(ListSM));
            D_INSTRUMENT.InsertNewHubs(HUB_INSTRUMENT.getSQNList(NewDims), NewDims, ListSM, ListSMFID);

            #endregion

            #region Update Dims
            List<string> NullableSymbolList = D_INSTRUMENT.getNullableSymbolCodeList(D_INSTRUMENT_List);
            List<HUB_INSTRUMENT> UpdateHubList = HUB_INSTRUMENT.getNullableHubList(NullableSymbolList, HUB_INSTRUMENTList);
            List<SAT_INSTRUMENT_SM> ListSMforUpdate = SAT_INSTRUMENT_SM.getSAT_SM(UpdateHubList);
            List<SAT_INSTRUMENT_SMFID> ListSMFIDforUpdate = SAT_INSTRUMENT_SMFID.getSAT_SMFID(UpdateHubList,SAT_INSTRUMENT_SM.getSQNList(ListSMforUpdate));
            D_INSTRUMENT.UpdateOldDims(HUB_INSTRUMENT.getSQNList(UpdateHubList),UpdateHubList, ListSMforUpdate, ListSMFIDforUpdate);
            #endregion


        }
    }
}
