﻿using ClassLibrary1.File;
using ClassLibrary1.Query;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;


namespace MainProgram
{
    public interface ISMtoDB
    {
        void SendSMtoDB(IFidelityFile file, string FileName);
    }


    public class SMtoDB : ISMtoDB
    {
        public void SendSMtoDB(IFidelityFile file, string FileName)
        {
            
            List<SAT_PRICE> ListSatPrice = new List<SAT_PRICE>();
            List<SAT_INSTRUMENT> ListSatInstrument = new List<SAT_INSTRUMENT>();
            List<HUB_PRICE> ListHubPrice = new List<HUB_PRICE>();
            List<HUB_INSTRUMENT> ListHubInstrument = new List<HUB_INSTRUMENT>();
            List<string> NewInstrumentCodes = ListHubPrice.Select(hub => hub.INSTRUMENT_CODE).ToList();
            List<string> NewCodes = ListHubInstrument.Select(hub => hub.CODE).ToList();
            List<string> InstHubswithoutSats = new List<string>();
            List<string> PriceHubswithoutSats = new List<string>();
            bool New_HUB;
            bool New_HUB2;

            Database.SetInitializer<data_vault_dev_db_01Entities2>(new CreateDatabaseIfNotExists<data_vault_dev_db_01Entities2>());

            using (data_vault_dev_db_01Entities2 ctx = new data_vault_dev_db_01Entities2())
            {

                List<HUB_INSTRUMENT> ExistingInstrumentHubs = ctx.HUB_INSTRUMENT.Select(m => m).ToList();
                List<HUB_PRICE> ExistingPriceHubs = ctx.HUB_PRICE.Select(m => m).ToList();
                List<SAT_PRICE> ExistingPriceSats = ctx.SAT_PRICE.Select(m => m).ToList();
                List<SAT_INSTRUMENT> ExistingInstrumentSats = ctx.SAT_INSTRUMENT.Select(m =>m).ToList();
                SAT_PRICE PreviousPRICESat = new SAT_PRICE();
                SAT_INSTRUMENT PreviousINSTRUMENTSat = new SAT_INSTRUMENT();

                List<KeyValuePair<string, string>> Code_CodeType = new List<KeyValuePair<string, string>>();
                foreach (var couple in ExistingInstrumentHubs)
                {
                    if (!Code_CodeType.Contains(new KeyValuePair<string, string>(couple.CODE, couple.CODE_TYPE)))
                        Code_CodeType.Add(new KeyValuePair<string, string>(couple.CODE, couple.CODE_TYPE));
                }

                List<KeyValuePair<string, string>> CodeInstrument_CodeType = new List<KeyValuePair<string, string>>();
                foreach (var couple in ExistingPriceHubs)
                {
                    if (!CodeInstrument_CodeType.Contains(new KeyValuePair<string, string>(couple.INSTRUMENT_CODE, couple.CODE_TYPE)))
                        CodeInstrument_CodeType.Add(new KeyValuePair<string, string>(couple.INSTRUMENT_CODE, couple.CODE_TYPE));
                }


                //List<KeyValuePair<long, string>> KeyHubInstrument = new List<KeyValuePair<long, string>>();
                //foreach (HUB_INSTRUMENT ExistingHub in ExistingInstrumentHubs)
                //{
                //    if (!(from hubs in KeyHubInstrument select hubs.Value).Contains(ExistingHub.CODE))
                //        KeyHubInstrument.Add(new KeyValuePair<long, string>(ExistingHub.SQN, ExistingHub.CODE));
                //}

                //// List<KeyValuePair<long, string>> KeyHubPrice = new List<KeyValuePair<long, string>>();
                //foreach (HUB_PRICE ExistingHub in ExistingPriceHubs)
                //{
                //    if (!(from hubs in KeyHubPrice select hubs.Value).Distinct().ToList().Contains(ExistingHub.INSTRUMENT_CODE))
                //        KeyHubPrice.Add(new KeyValuePair<long, string>(ExistingHub.SQN, ExistingHub.INSTRUMENT_CODE));
                //}




                foreach (FidelitySMFileLine line in file.List1())
                {



                    #region Preparing JSON

                    IHub_Instrument Hub_Instrument = new Hub_Instrument(line);
                    ISat_Instrument_SM Sat_Instrument_SM = new Sat_Instrument_SM(line);
                    IConvertToJSON Convert = new ConvertToJSON();
                    string Sat_Instrument_SMJSON = Convert.ConvertingToJson(Sat_Instrument_SM);
                    IHub_Price Hub_Price = new Hub_Price(line);
                    ISat_Price_SM Sat_Price_SM = new Sat_Price_SM(line);
                    string Sat_Price_SMJSON = Convert.ConvertingToJson(Sat_Price_SM);

                    HUB_INSTRUMENT HUB_INSTRUMENT = new HUB_INSTRUMENT();
                    SAT_INSTRUMENT SAT_INSTRUMENT = new SAT_INSTRUMENT();
                    HUB_PRICE HUB_PRICE = new HUB_PRICE();
                    SAT_PRICE SAT_PRICE = new SAT_PRICE();

                    #endregion

                    #region HUB_INSTRUMENT
                    HUB_INSTRUMENT.RECORD_SRC = Hub_Instrument.GetRecordSRC();
                    HUB_INSTRUMENT.LOAD_DATE = DateTime.ParseExact(System.DateTime.Now.ToString("yyyyMMdd hh:mm:ss:FFFF"), "yyyyMMdd hh:mm:ss:FFFF", CultureInfo.InvariantCulture);
                    HUB_INSTRUMENT.CODE = Hub_Instrument.GetCode();
                    HUB_INSTRUMENT.CODE_TYPE = Hub_Instrument.GetCodeType();


                    KeyValuePair<string, string> InstKey = new KeyValuePair<string, string>(HUB_INSTRUMENT.CODE, HUB_INSTRUMENT.CODE_TYPE);
                    New_HUB = !Code_CodeType.Contains(InstKey);
                    
                    if (New_HUB)
                    {
                        ExistingInstrumentHubs.Add(HUB_INSTRUMENT);
                        ListHubInstrument.Add(HUB_INSTRUMENT);
                        // KeyHubInstrument.Add(new KeyValuePair<long, string>(HUB_INSTRUMENT.SQN, HUB_INSTRUMENT.CODE));
                        Code_CodeType.Add(InstKey);
                        NewInstrumentCodes.Add(HUB_INSTRUMENT.CODE);
                    }
                    //ListHubInstrument = ListHubInstrument.GroupBy(hub => hub.CODE).Select(hub => hub.OrderByDescending(x => x.LOAD_DATE).First()).Distinct().ToList();
                    #endregion
                    #region SAT_INSTRUMENT
                    SAT_INSTRUMENT.RECORD_SRC = "SM";
                    SAT_INSTRUMENT.LOAD_DATE = DateTime.ParseExact(System.DateTime.Now.ToString("yyyyMMdd hh:mm:ss:FFFF"), "yyyyMMdd hh:mm:ss:FFFF", CultureInfo.InvariantCulture);
                    SAT_INSTRUMENT.RECORD = Sat_Instrument_SMJSON;
                    SAT_INSTRUMENT.RECORD_DATE = new DateTime(Int32.Parse(FileName.Substring(2,2))+2000, Int32.Parse(FileName.Substring(4, 2)), Int32.Parse(FileName.Substring(6, 2)));
                    SAT_INSTRUMENT.SATELLITE_TYPE = "SM";
                    //SAT_INSTRUMENT.HUB_INSTRUMENT = ListHubInstrument.Where(hub => hub.CODE == HUB_INSTRUMENT.CODE).First();
                    //PreviousINSTRUMENTSat = ListHubInstrument.Where(hub => hub.CODE == HUB_INSTRUMENT.CODE).First().SAT_INSTRUMENT.Where(sat => sat.RECORD_SRC == "SM").OrderByDescending(sat => sat.LOAD_DATE).FirstOrDefault();
                    if (New_HUB)
                    {
                        ListHubInstrument.Where(hub => hub.CODE == HUB_INSTRUMENT.CODE).First().SAT_INSTRUMENT.Add(SAT_INSTRUMENT);
                    }

                    else
                    {
                        long Actual_SQN = ExistingInstrumentHubs.Where(hub => hub.CODE == HUB_INSTRUMENT.CODE).First().SQN;
                        PreviousINSTRUMENTSat = ExistingInstrumentSats.Where(sat => sat.SQN == Actual_SQN && sat.RECORD_SRC=="SM").OrderByDescending(sat => sat.LOAD_DATE).FirstOrDefault(); ;
                        if (PreviousPRICESat != null )

                        {
                            if (SAT_INSTRUMENT.RECORD != PreviousINSTRUMENTSat.RECORD && SAT_INSTRUMENT.RECORD_DATE != PreviousINSTRUMENTSat.RECORD_DATE)
                            {
                                if (NewInstrumentCodes.Contains(HUB_INSTRUMENT.CODE))
                                {
                                    ListHubInstrument.Where(hub => hub.CODE == HUB_INSTRUMENT.CODE).First().SAT_INSTRUMENT.Add(SAT_INSTRUMENT);
                                }
                                else
                                {
                                    HUB_INSTRUMENT Actual_Hub = ExistingInstrumentHubs.Where(hub => hub.CODE == HUB_INSTRUMENT.CODE).First();
                                    ctx.HUB_INSTRUMENT.Where(hub => hub == Actual_Hub).First().SAT_INSTRUMENT.Add(SAT_INSTRUMENT);
                                    ctx.SaveChanges();
                                }
                            }
                        }
                        else
                        {
                            HUB_INSTRUMENT PreviousHubInstrument = ListHubInstrument.Where(hub => hub.CODE == HUB_INSTRUMENT.CODE).FirstOrDefault();
                            if (PreviousHubInstrument != null)
                            {
                                SAT_INSTRUMENT PreviousSatInUninsertedHub = PreviousHubInstrument.SAT_INSTRUMENT.Where(sat => sat.SATELLITE_TYPE == "SM").OrderBy(sat => sat.LOAD_DATE).FirstOrDefault();
                                if (PreviousSatInUninsertedHub != null)
                                {
                                    if ((PreviousSatInUninsertedHub.RECORD != SAT_INSTRUMENT.RECORD && PreviousSatInUninsertedHub.RECORD_DATE < SAT_INSTRUMENT.RECORD_DATE))
                                    {
                                        ListHubInstrument.Where(hub => hub.CODE == HUB_INSTRUMENT.CODE).First().SAT_INSTRUMENT.Add(SAT_INSTRUMENT);

                                    }
                                }
                            }
                            else
                            {
                                if (!InstHubswithoutSats.Contains(HUB_INSTRUMENT.CODE))
                                {
                                    ctx.HUB_INSTRUMENT.Where(hub => hub.CODE == HUB_INSTRUMENT.CODE).First().SAT_INSTRUMENT.Add(SAT_INSTRUMENT);
                                    ctx.SaveChanges();
                                    InstHubswithoutSats.Add(HUB_INSTRUMENT.CODE);
                                }
                            }
                        }
                    }
                    if (!ExistingInstrumentSats.Contains(SAT_INSTRUMENT))
                    {
                        ExistingInstrumentSats.Add(SAT_INSTRUMENT);
                    }
                    #endregion
                    #region HUB_PRICE
                    HUB_PRICE.PRICE_DTS = line.PriceDate().AddHours(16);
                    HUB_PRICE.LOAD_DATE = DateTime.ParseExact(System.DateTime.Now.ToString("yyyyMMdd hh:mm:ss:FFFF"), "yyyyMMdd hh:mm:ss:FFFF", CultureInfo.InvariantCulture);
                    HUB_PRICE.INSTRUMENT_CODE = Hub_Price.GetInstrumentCode();
                    HUB_PRICE.CODE_TYPE = Hub_Price.GetCodeType();
                    HUB_PRICE.RECORD_SRC = Hub_Price.GetRecordSRC();


                    KeyValuePair<string, string> PriceKey = new KeyValuePair<string, string>(HUB_PRICE.INSTRUMENT_CODE, HUB_PRICE.CODE_TYPE);
                    New_HUB2 = !CodeInstrument_CodeType.Contains(PriceKey);



                    if (New_HUB2)
                    {
                        ExistingPriceHubs.Add(HUB_PRICE);
                        ListHubPrice.Add(HUB_PRICE);
                        //KeyHubPrice.Add(new KeyValuePair<long, string>(HUB_PRICE.SQN, HUB_PRICE.INSTRUMENT_CODE));
                        CodeInstrument_CodeType.Add(PriceKey);
                        NewInstrumentCodes.Add(HUB_PRICE.INSTRUMENT_CODE);
                    }
                    ListHubPrice = ListHubPrice.GroupBy(hub => hub.INSTRUMENT_CODE).Select(hub => hub.OrderByDescending(x => x.LOAD_DATE).First()).Distinct().ToList();
                    #endregion
                    #region SAT_PRICE
                    SAT_PRICE.LOAD_DATE = DateTime.ParseExact(System.DateTime.Now.ToString("yyyyMMdd hh:mm:ss:FFFF"), "yyyyMMdd hh:mm:ss:FFFF", CultureInfo.InvariantCulture);
                    SAT_PRICE.RECORD = Sat_Price_SMJSON;
                    SAT_PRICE.RECORD_DATE = new DateTime(Int32.Parse(FileName.Substring(2, 2)) + 2000, Int32.Parse(FileName.Substring(4, 2)), Int32.Parse(FileName.Substring(6, 2)));
                    SAT_PRICE.SATELLITE_TYPE = "SM";
                    SAT_PRICE.RECORD_SRC = "SM";
                   // PreviousPRICESat = ListHubPrice.Where(hub => hub.INSTRUMENT_CODE == HUB_PRICE.INSTRUMENT_CODE).First().SAT_PRICE.Where(sat => sat.RECORD_SRC == "SM").OrderByDescending(sat => sat.LOAD_DATE).FirstOrDefault();

                    if (New_HUB2)
                    {
                        ListHubPrice.Where(hub => hub.INSTRUMENT_CODE == HUB_PRICE.INSTRUMENT_CODE).First().SAT_PRICE.Add(SAT_PRICE);

                    }

                    else
                    {
                        long Actual_SQN = ExistingPriceHubs.Where(hub => hub.INSTRUMENT_CODE == HUB_PRICE.INSTRUMENT_CODE).First().SQN;
                        PreviousPRICESat = ExistingPriceSats.Where(sat => sat.SQN == Actual_SQN && sat.SATELLITE_TYPE == "SM").OrderByDescending(sat => sat.LOAD_DATE).FirstOrDefault(); ;
                        if (PreviousPRICESat != null )

                        {
                            if (SAT_PRICE.RECORD != PreviousPRICESat.RECORD && SAT_PRICE.RECORD_DATE != PreviousPRICESat.RECORD_DATE)
                            {
                                if(NewInstrumentCodes.Contains(HUB_PRICE.INSTRUMENT_CODE))
                                {
                                    ListHubPrice.Where(hub => hub.INSTRUMENT_CODE == HUB_PRICE.INSTRUMENT_CODE).First().SAT_PRICE.Add(SAT_PRICE);
                                }
                                else
                                {
                                    HUB_PRICE Actual_Hub = ExistingPriceHubs.Where(hub => hub.INSTRUMENT_CODE == HUB_PRICE.INSTRUMENT_CODE).First();
                                    ctx.HUB_PRICE.Where(hub => hub == Actual_Hub).First().SAT_PRICE.Add(SAT_PRICE);
                                    ctx.SaveChanges();
                                }
                            }
                        }
                        else
                        {
                            HUB_PRICE PreviousHubPrice = ListHubPrice.Where(hub => hub.INSTRUMENT_CODE == HUB_PRICE.INSTRUMENT_CODE).FirstOrDefault();
                            if (PreviousHubPrice != null)
                            {
                                SAT_PRICE PreviousSatInUninsertedHub = PreviousHubPrice.SAT_PRICE.Where(sat => sat.SATELLITE_TYPE == "SM").OrderBy(sat => sat.LOAD_DATE).FirstOrDefault();
                                if (PreviousSatInUninsertedHub != null)
                                {
                                    if ((PreviousSatInUninsertedHub.RECORD != SAT_PRICE.RECORD && PreviousSatInUninsertedHub.RECORD_DATE < SAT_PRICE.RECORD_DATE))
                                    {
                                        ListHubPrice.Where(hub => hub.INSTRUMENT_CODE == HUB_PRICE.INSTRUMENT_CODE).First().SAT_PRICE.Add(SAT_PRICE);

                                    }
                                }
                            }
                            else
                            {
                                if (!PriceHubswithoutSats.Contains(HUB_PRICE.INSTRUMENT_CODE))
                                {
                                    ctx.HUB_PRICE.Where(hub => hub.INSTRUMENT_CODE == HUB_PRICE.INSTRUMENT_CODE).First().SAT_PRICE.Add(SAT_PRICE);
                                    ctx.SaveChanges();
                                    PriceHubswithoutSats.Add(HUB_PRICE.INSTRUMENT_CODE);
                                }
                            }
                        }
                    }
                    if (!ExistingPriceSats.Contains(SAT_PRICE))
                    {
                        ExistingPriceSats.Add(SAT_PRICE);
                    }
                    
                    #endregion

                }
                
                ctx.HUB_INSTRUMENT.AddRange(ListHubInstrument);
                ctx.HUB_PRICE.AddRange(ListHubPrice);
                ctx.SaveChanges();


            }
        }
    }
}



