﻿using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;
using System.Data.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using ClassLibrary1.File;

namespace MainProgram
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Connecting to Blob
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer SMcontainer = blobClient.GetContainerReference("dataphile-sm");
            CloudBlobContainer SM2container = blobClient.GetContainerReference("dataphile-sm2");
            SMcontainer.CreateIfNotExists();
            // lire le fichier du blob
            CloudBlockBlob blockBlob = SMcontainer.GetBlockBlobReference("sm150105");
            SM2container.CreateIfNotExists();
            // lire le fichier du blob
            CloudBlockBlob SMblockBlob = SM2container.GetBlockBlobReference("sm2150105.JIT");
            string FileName = "sm150105";
            string FileName2 = "sm2150105.JIT";
            MemoryStream memoryStream;
            using (memoryStream = new MemoryStream())

            { blockBlob.DownloadToStream(memoryStream); }


            MemoryStream memoryStream2;
            using (memoryStream2 = new MemoryStream())

            { SMblockBlob.DownloadToStream(memoryStream2); }





            #endregion


            #region Inserting SM and SMFID into DB



            List<MemoryStream> MemoryList = new List<MemoryStream>();

            MemoryList.Add(memoryStream);
            MemoryList.Add(memoryStream2);
            IFidelityFile file = new FidelityMemoryStream(memoryStream);
            IFidelityFile file2 = new FidelityMemoryStream(memoryStream2);

            //Parallel.ForEach(MemoryList, (Memory) =>
            // {
            //     if (Memory == memoryStream)
            //     {
                     ISMtoDB SMtoDB = new SMtoDB();
                     SMtoDB.SendSMtoDB(file, FileName);
                 //}
                 
                 //else
                 //{
                     ISMFIDtoDB SMFIDtoDB = new SMFIDtoDB();
                     SMFIDtoDB.SendSMFIDtoDB(file2, FileName2);
             //    }
             //});
            #endregion
        }
    }
}
