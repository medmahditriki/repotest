﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.File
{
    public class FidelitySMFIDFileLine : IFidelitySMFIDFileLine
    {
        private string _line;
        public FidelitySMFIDFileLine(string line)
        {
            _line = line;
        }

        public string SecurityCode()
        {
            return new FidelityString(_line, 1, 6).Value();
        }
        public double ClassCode()
        {
            return new FidelityDouble(new FidelityString(_line, 7, 3)).Value();
        }
        public string SecurityName()
        {
            return new FidelityString(_line, 10, 29).Value();
        }
        public string Description()
        {
            return new FidelityString(_line, 39, 58).Value();
        }
        public string CurrencyCode1()
        {
            return new FidelityString(_line, 97, 3).Value();
        }
        public double PriceFactor()
        {
            return new DecimalsApplied(new FidelityDouble(new FidelityString(_line, 100, 11)), 5).Value();
        }
        public string CusipNo()
        {
            return new FidelityString(_line, 111, 9).Value();
        }
        public string Symbol()
        {
            return new FidelityString(_line, 120, 9).Value();
        }
        public string RRSPEligibiltyFlag()
        {
            return new FidelityString(_line, 129, 1).Value();
        }
        public string CCPCEligibiltyFlag()
        {
            return new FidelityString(_line, 130, 1).Value();
        }
        public double DividendReinvestmentEligibleFlag()
        {
            return new FidelityDouble(new FidelityString(_line, 131, 1)).Value();
        }
        public string Status()
        {
            return new FidelityString(_line, 132, 1).Value();
        }
        public double LastTradePrice()
        {
            return new DecimalsApplied(new FidelityDouble(new FidelityString(_line, 133, 11)), 5).Value();
        }
        public string MarketCode()
        {
            return new FidelityString(_line, 144, 2).Value();
        }
        public DateTime PriceDate()
        {
            string strDate = new FidelityString(_line, 146, 8).Value();

            DateTime dateTime;

            bool success = DateTime.TryParseExact(strDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);

            if (!success)
            {
                return new DateTime();
            }
            else
            {
                return dateTime;
            }

        }
        public double AlternateMarketPrice()
        {
            return new DecimalsApplied(new FidelityDouble(new FidelityString(_line, 154, 11)), 5).Value();
        }
        public string AlternateMarketCode()
        {
            return new FidelityString(_line, 165, 2).Value();
        }
        public DateTime AlternateMarketPriceDate()
        {
            string strDate = new FidelityString(_line, 167, 8).Value();

            DateTime dateTime;

            bool success = DateTime.TryParseExact(strDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);

            if (!success)
            {
                return new DateTime();
            }
            else
            {
                return dateTime;
            }

        }
        public string Industry()
        {
            return new FidelityString(_line, 175, 8).Value();
        }
        public string Sector()
        {
            return new FidelityString(_line, 183, 8).Value();
        }
        public string SubIndustry()
        {
            return new FidelityString(_line, 191, 8).Value();
        }
        public double InterestRate()
        {
            return new DecimalsApplied(new FidelityDouble(new FidelityString(_line, 199, 8)), 4).Value();
        }
        public double ZeroConstant1()
        {
            return new FidelityDouble(new FidelityString(_line, 207, 8)).Value();
        }
        public double ZeroConstant2()
        {
            return new FidelityDouble(new FidelityString(_line, 215, 8)).Value();
        }
        public DateTime ExpiryDate1()
        {
            string strDate = new FidelityString(_line, 223, 8).Value();

            DateTime dateTime;

            bool success = DateTime.TryParseExact(strDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);

            if (!success)
            {
                return new DateTime();
            }
            else
            {
                return dateTime;
            }

        }
        public string BlackConstant1()
        {
            return new FidelityString(_line, 231, 8).Value();
        }
        public string BlackConstant2()
        {
            return new FidelityString(_line, 239, 8).Value();
        }
        public DateTime AccuralDate()
        {
            string strDate = new FidelityString(_line, 247, 8).Value();

            DateTime dateTime;

            bool success = DateTime.TryParseExact(strDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);

            if (!success)
            {
                return new DateTime();
            }
            else
            {
                return dateTime;
            }

        }
        public string PaymentFrequency()
        {
            return new FidelityString(_line, 255, 1).Value();
        }
        public string OptionsFlag()
        {
            return new FidelityString(_line, 256, 1).Value();
        }
        public string CouponsFlag()
        {
            return new FidelityString(_line, 257, 1).Value();
        }




        public DateTime ExpiryDate2()
        {
            string strDate = new FidelityString(_line, 258, 8).Value();

            DateTime dateTime;

            bool success = DateTime.TryParseExact(strDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);

            if (!success)
            {
                return new DateTime();
            }
            else
            {
                return dateTime;
            }

        }
        public double StrikePrice()
        {
            return new DecimalsApplied(new FidelityDouble(new FidelityString(_line, 266, 11)), 5).Value();
        }
        public string UnderlyingCusip()
        {
            return new FidelityString(_line, 277, 9).Value();
        }
        public string OSCExemptFlag()
        {
            return new FidelityString(_line, 286, 1).Value();
        }
        public string OptionCallPut()
        {
            return new FidelityString(_line, 287, 1).Value();
        }
        public string DiscountNoteFlag()
        {
            return new FidelityString(_line, 288, 1).Value();
        }
        public string FixedRateFlag()
        {
            return new FidelityString(_line, 289, 1).Value();
        }
        public DateTime FirstCouponDate()
        {
            string strDate = new FidelityString(_line, 290, 8).Value();

            DateTime dateTime;

            bool success = DateTime.TryParseExact(strDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);

            if (!success)
            {
                return new DateTime();
            }
            else
            {
                return dateTime;
            }

        }
        public string BBSEligibleFlag()
        {
            return new FidelityString(_line, 298, 1).Value();
        }
        public string CDSEligibleFlag()
        {
            return new FidelityString(_line, 299, 1).Value();
        }
        public string CNSEligibleFlag()
        {
            return new FidelityString(_line, 300, 1).Value();
        }
        public string VSEEligibleFlag()
        {
            return new FidelityString(_line, 301, 1).Value();
        }
        public string MDWEligibleFlag()
        {
            return new FidelityString(_line, 302, 1).Value();
        }
        public string EUREligibleFlag()
        {
            return new FidelityString(_line, 303, 1).Value();
        }
        public string QSSPEligible()
        {
            return new FidelityString(_line, 304, 1).Value();
        }
        public string NIDSEligibleFlag()
        {
            return new FidelityString(_line, 305, 1).Value();
        }
        public string DCSECSEligibleFlag()
        {
            return new FidelityString(_line, 306, 1).Value();
        }
        public string CSSEligible()
        {
            return new FidelityString(_line, 307, 1).Value();
        }
        public string QSCEligibleFlag()
        {
            return new FidelityString(_line, 308, 1).Value();
        }
        public string CertificateNumber()
        {
            return new FidelityString(_line, 309, 12).Value();
        }
        public DateTime PayableDate()
        {
            string strDate = new FidelityString(_line, 321, 8).Value();

            DateTime dateTime;

            bool success = DateTime.TryParseExact(strDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);

            if (!success)
            {
                return new DateTime();
            }
            else
            {
                return dateTime;
            }

        }
        public double DividendRate()
        {
            return new DecimalsApplied(new FidelityDouble(new FidelityString(_line, 329, 8)), 4).Value();
        }
        public string CurrencyCode2()
        {
            return new FidelityString(_line, 337, 1).Value();
        }
        public DateTime RecordDate()
        {
            string strDate = new FidelityString(_line, 338, 8).Value();

            DateTime dateTime;

            bool success = DateTime.TryParseExact(strDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);

            if (!success)
            {
                return new DateTime();
            }
            else
            {
                return dateTime;
            }

        }
        public DateTime ExDividendDate()
        {
            string strDate = new FidelityString(_line, 346, 8).Value();

            DateTime dateTime;

            bool success = DateTime.TryParseExact(strDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);

            if (!success)
            {
                return new DateTime();
            }
            else
            {
                return dateTime;
            }

        }
        public double DividendYield()
        {
            return new DecimalsApplied(new FidelityDouble(new FidelityString(_line, 354, 9)), 4).Value();
        }
        public string LongSymbol()
        {
            return new FidelityString(_line, 363, 17).Value();
        }
        public string DayCountBasis()
        {
            return new FidelityString(_line, 380, 1).Value();
        }
        public string CountryCode()
        {
            return new FidelityString(_line, 381, 3).Value();
        }
        public string EnglishShortSecurityName()
        {
            return new FidelityString(_line, 384, 35).Value();
        }
        public string EnglishLongSecurityName()
        {
            return new FidelityString(_line, 419, 150).Value();
        }
        public string FrenchShortSecurityName()
        {
            return new FidelityString(_line, 569, 35).Value();
        }
        public string FrenchLongSecurityName()
        {
            return new FidelityString(_line, 604, 150).Value();
        }



    }
}
