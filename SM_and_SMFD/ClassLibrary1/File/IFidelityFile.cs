﻿using System.Collections.Generic;

namespace ClassLibrary1.File
{
    public interface IFidelityFile
    {
        IList<IFidelitySMFileLine> List1();
        IList<IFidelitySMFIDFileLine> List2();
    }
}
