﻿using System;

namespace ClassLibrary1.File
{
    public interface IFidelitySMFileLine
    {

        //DateTime PositionDate();
        string CustomerCode();
        string CusipNo();
        string SecurityName();
        string CurrencyCode();
        double BidPrice();

        double AskPrice();
        double LastTradePrice();
        DateTime PriceDate();
        string PrimaryMarketCode();
        string Symbol();
        string MarketCode2();
        string Symbol2();
        string MarketCode3();
        string Symbol3();
        string MarketCode4();
        string Symbol4();
        string MarketCode5();
        string Symbol5();
        string MarketCode6();
        string Symbol6();
        string MarketCode7();
        string Symbol7();
        string ClassCode();
        string Status();
        string OptionsFlag();
        string ReducedMarginCode();
        DateTime ExpiryDate();
        string BBSEligibleFlag();
        string CDSEligibleFlag();
        string CNSEligibleFlag();
        string DTCEligibleFlag();
        string DCSECSEligibleFlag();
        string RRSPEligible();
        string UnderlyingCusip();
        double PriceFactor();
        string CountryCode();
        string LongSymbol();
















    }
}
