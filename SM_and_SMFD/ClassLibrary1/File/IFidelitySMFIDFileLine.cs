﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.File
{
    public interface IFidelitySMFIDFileLine
    {
        string SecurityCode();
        double ClassCode();
        string SecurityName();
        string Description();
        string CurrencyCode1();
        double PriceFactor();
        string CusipNo();
        string Symbol();
        string RRSPEligibiltyFlag();
        string CCPCEligibiltyFlag();
        double DividendReinvestmentEligibleFlag();
        string Status();
        double LastTradePrice();
        string MarketCode();
        DateTime PriceDate();
        double AlternateMarketPrice();
        string AlternateMarketCode();
        DateTime AlternateMarketPriceDate();
        string Industry();
        string Sector();
        string SubIndustry();
        double InterestRate();
        double ZeroConstant1();
        double ZeroConstant2();
        DateTime ExpiryDate1();
        string BlackConstant1();
        string BlackConstant2();
        DateTime AccuralDate();
        string PaymentFrequency();
        string OptionsFlag();
        string CouponsFlag();
        DateTime ExpiryDate2();
        double StrikePrice();
        string UnderlyingCusip();
        string OSCExemptFlag();
        string OptionCallPut();
        string DiscountNoteFlag();
        string FixedRateFlag();
        DateTime FirstCouponDate();
        string BBSEligibleFlag();
        string CDSEligibleFlag();
        string CNSEligibleFlag();
        string VSEEligibleFlag();
        string MDWEligibleFlag();
        string EUREligibleFlag();
        string QSSPEligible();
        string NIDSEligibleFlag();
        string DCSECSEligibleFlag();
        string CSSEligible();
        string QSCEligibleFlag();
        string CertificateNumber();
        DateTime PayableDate();
        double DividendRate();
        string CurrencyCode2();
        DateTime RecordDate();
        DateTime ExDividendDate();
        double DividendYield();
        string LongSymbol();
        string DayCountBasis();
        string CountryCode();
        string EnglishShortSecurityName();
        string EnglishLongSecurityName();
        string FrenchShortSecurityName();
        string FrenchLongSecurityName();




    }
}
