﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.File
{
    public class FidelitySMFileLine : IFidelitySMFileLine
    {
        private string _line;
        public FidelitySMFileLine(string line)
        {
            _line = line;
        }
        public string CustomerCode()
        {
            return new FidelityString(_line, 1, 3).Value();
        }
        public string CusipNo()
        {
            return new FidelityString(_line, 4, 9).Value();
        }
        public string SecurityName()
        {
            return new FidelityString(_line, 13, 35).Value();
        }
        public string CurrencyCode()
        {
            return new FidelityString(_line, 48, 3).Value();
        }
        public double BidPrice()
        {
            return new DecimalsApplied(new FidelityDouble(new FidelityString(_line, 51, 19)), 8).Value();
        }
        public double AskPrice()
        {
            return new DecimalsApplied(new FidelityDouble(new FidelityString(_line, 70, 19)), 8).Value();
        }
        public double LastTradePrice()
        {
            return new DecimalsApplied(new FidelityDouble(new FidelityString(_line,89,19)), 8).Value();
        }
        public DateTime PriceDate()
        {
            string strDate = new FidelityString(_line, 108, 8).Value();

            DateTime dateTime;

            bool success = DateTime.TryParseExact(strDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);

            if (!success)
            {
                return new DateTime();
            }
            else
            {
                return dateTime;
            }

        }
        public string PrimaryMarketCode()
        {
            return new FidelityString(_line, 116, 2).Value();
        }
        public string Symbol()
        {
            return new FidelityString(_line, 118, 8).Value();
        }
        public string MarketCode2()
        {
            return new FidelityString(_line, 126, 2).Value();
        }
        public string Symbol2()
        {
            return new FidelityString(_line, 128, 8).Value();
        }
        public string MarketCode3()
        {
            return new FidelityString(_line, 136, 2).Value();
        }
        public string Symbol3()
        {
            return new FidelityString(_line, 138, 8).Value();
        }
        public string MarketCode4()
        {
            return new FidelityString(_line, 146, 2).Value();
        }
        public string Symbol4()
        {
            return new FidelityString(_line, 148, 8).Value();
        }
        public string MarketCode5()
        {
            return new FidelityString(_line, 156, 2).Value();
        }
        public string Symbol5()
        {
            return new FidelityString(_line, 158, 8).Value();
        }
        public string MarketCode6()
        {
            return new FidelityString(_line, 166, 2).Value();
        }
        public string Symbol6()
        {
            return new FidelityString(_line, 168, 8).Value();
        }
        public string MarketCode7()
        {
            return new FidelityString(_line, 176, 2).Value();
        }
        public string Symbol7()
        {
            return new FidelityString(_line, 178, 8).Value();
        }
        public string ClassCode()
        {
            return new FidelityString(_line, 186, 3).Value();
        }
        public string Status()
        {
            return new FidelityString(_line, 189, 1).Value();
        }
        public string OptionsFlag()
        {
            return new FidelityString(_line, 190, 1).Value();
        }
        public string ReducedMarginCode()
        {
            return new FidelityString(_line, 191, 1).Value();
        }
        public DateTime ExpiryDate()
        {
            string strDate = new FidelityString(_line, 192, 8).Value();

            DateTime dateTime;

            bool success = DateTime.TryParseExact(strDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);

            if (!success)
            {
                return new DateTime();
            }
            else
            {
                return dateTime;
            }

        }
        public string BBSEligibleFlag()
        {
            return new FidelityString(_line, 200, 1).Value();
        }
        public string CDSEligibleFlag()
        {
            return new FidelityString(_line, 201, 1).Value();
        }
        public string CNSEligibleFlag()
        {
            return new FidelityString(_line, 202, 1).Value();
        }
        public string DTCEligibleFlag()
        {
            return new FidelityString(_line, 203, 1).Value();
        }
        public string DCSECSEligibleFlag()
        {
            return new FidelityString(_line, 204, 1).Value();
        }
        public string RRSPEligible()
        {
            return new FidelityString(_line, 205, 1).Value();
        }
        public string UnderlyingCusip()
        {
            return new FidelityString(_line, 206, 9).Value();
        }
        public double PriceFactor()
        {
            return new DecimalsApplied(new FidelityDouble(new FidelityString(_line, 215, 10)), 5).Value();
        }
        public string CountryCode()
        {
            return new FidelityString(_line, 225, 3).Value();
        }
        public string LongSymbol()
        {
            return new FidelityString(_line, 228, 17).Value();
        }





    }
}
