﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ClassLibrary1.File
{
    public class FidelityFile : IFidelityFile
    {

        private FileInfo _fileInfo;

        public FidelityFile(FileInfo fileInfo)
        {
            _fileInfo = fileInfo;
        }

        public IList<IFidelitySMFileLine> List1()
        {
            IList<IFidelitySMFileLine> list = new List<IFidelitySMFileLine>();
            using (StreamReader reader = _fileInfo.OpenText())
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    list.Add(new FidelitySMFileLine(line));
                }
            }

            return list;
        }
        public IList<IFidelitySMFIDFileLine> List2()
        {
            IList<IFidelitySMFIDFileLine> list = new List<IFidelitySMFIDFileLine>();
            using (StreamReader reader = _fileInfo.OpenText())
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    list.Add(new FidelitySMFIDFileLine(line));
                }
            }

            return list;
        }

    }


    public class FidelityMemoryStream : IFidelityFile
    {
        private MemoryStream _stream;

        public FidelityMemoryStream(MemoryStream stream)
        {
            _stream = stream;
        }

        public IList<IFidelitySMFileLine> List1()
        {
            IList<IFidelitySMFileLine> list = new List<IFidelitySMFileLine>();

            string text = System.Text.Encoding.UTF8.GetString(_stream.ToArray());

            string[] array = text.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string line in array)
            {
                list.Add(new FidelitySMFileLine(line));
            }

            return list;
        }
        public IList<IFidelitySMFIDFileLine> List2()
        {
            IList<IFidelitySMFIDFileLine> list = new List<IFidelitySMFIDFileLine>();

            string text = System.Text.Encoding.UTF8.GetString(_stream.ToArray());

            string[] array = text.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string line in array)
            {
                list.Add(new FidelitySMFIDFileLine(line));
            }

            return list;
        }
    }

    //public class AccountFilteredFidelityFile : IFidelityFile
    //{
    //    IFidelityFile _origin;
        

    //    public AccountFilteredFidelityFile(IFidelityFile origin, string accountNo)
    //    {
    //        _origin = origin;
           
    //    }

    //    //public IList<IFidelitySMFileLine> List()
    //    //{


    //    //    return _origin.List().Where(k => k.AccountNo() == _accountNo).ToList();
    //    //}
    //}
}
