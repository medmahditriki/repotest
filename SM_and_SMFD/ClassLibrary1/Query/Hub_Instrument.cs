﻿using ClassLibrary1.File;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Query
{
   public class Hub_Instrument : IHub_Instrument
    {
        public string Code;
        public string CodeType;
        public string RecordSRC;
        public Hub_Instrument(FidelitySMFileLine line)
        {
            Code = line.CusipNo();
            CodeType = "CUSIP";
            RecordSRC = "SM";
        }
        public Hub_Instrument(FidelitySMFIDFileLine line)
        {
            Code = line.CusipNo();
            CodeType = "CUSIP";
            RecordSRC = "SMFID";
        }
        public string GetCode()
        {
            return Code;
        }
        public string GetCodeType()
        {
            return CodeType;
        }
        public string GetRecordSRC()
        {
            return RecordSRC;
        }
    }
}
