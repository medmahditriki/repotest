﻿using ClassLibrary1.File;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Query
{
    public class Sat_Instrument_SM : ISat_Instrument_SM
    {
        public string SecurityName;
        public string CurrencyCode;
        public string PrimaryMarketCode;
        public string Symbol;
        public string MarketCode2;
        public string Symbol2;
        public string MarketCode3;
        public string Symbol3;
        public string MarketCode4;
        public string Symbol4;
        public string MarketCode5;
        public string Symbol5;
        public string MarketCode6;
        public string Symbol6;
        public string MarketCode7;
        public string Symbol7;
        public string ClassCode;
        public string Status;
        public string OptionFlag;
        public string ReducedMarginCode;
        public DateTime ExpiryDate;
        public string BBSEligibleFlag;
        public string CDSEligibleFlag;
        public string DCSECSEligibleFlag;
        public string RRSPEligibleFlag;
        public string UnderlyingCusip;
        public double PriceFactor;
        public string CountryCode;
        public string SymbolLong;
        public Sat_Instrument_SM(FidelitySMFileLine line)
        {
            SecurityName = line.SecurityName();
            CurrencyCode = line.CurrencyCode();
            PrimaryMarketCode = line.PrimaryMarketCode();
            Symbol = line.Symbol();
            MarketCode2 = line.MarketCode2();
            Symbol2 = line.Symbol2();
            MarketCode3 = line.MarketCode3();
            Symbol3 = line.Symbol3();
            MarketCode4 = line.MarketCode4();
            Symbol4 = line.Symbol4();
            MarketCode5 = line.MarketCode5();
            Symbol5 = line.Symbol5();
            MarketCode6 = line.MarketCode6();
            Symbol6 = line.Symbol6();
            MarketCode7 = line.MarketCode7();
            Symbol7 = line.Symbol7();
            ClassCode = line.ClassCode();
            Status = line.Status();
            OptionFlag = line.OptionsFlag();
            ReducedMarginCode = line.ReducedMarginCode();
            ExpiryDate = line.ExpiryDate();
            BBSEligibleFlag = line.BBSEligibleFlag();
            CDSEligibleFlag = line.CDSEligibleFlag();
            DCSECSEligibleFlag = line.DCSECSEligibleFlag();
            RRSPEligibleFlag = line.RRSPEligible();
            UnderlyingCusip = line.UnderlyingCusip();
            PriceFactor = line.PriceFactor();
            CountryCode = line.CountryCode();
            SymbolLong = line.LongSymbol();
        }

    }
}
