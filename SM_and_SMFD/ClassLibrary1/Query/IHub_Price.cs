﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Query
{
    public interface IHub_Price
    {
        string GetInstrumentCode();
        string GetCodeType();
        string GetRecordSRC();
        DateTime GetPriceDTS();
    }
}
