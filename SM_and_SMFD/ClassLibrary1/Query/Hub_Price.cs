﻿using ClassLibrary1.File;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Query
{
    public class Hub_Price : IHub_Price
    {
        public string InstrumentCode;
        public string CodeType = "CUSIP";
        public DateTime PriceDTS;
        public string RecordSRC;
        public Hub_Price(FidelitySMFileLine line)
        {
            InstrumentCode = line.CusipNo();
           
            RecordSRC = "SM";
        }
        public Hub_Price(FidelitySMFIDFileLine line)
        {
            InstrumentCode = line.CusipNo();
            RecordSRC = "SMFID";
            
        }
        public string GetInstrumentCode()
        {
            return InstrumentCode;
        }
        public string GetCodeType()
        {
            return CodeType;
        }
        public string GetRecordSRC()
        {
            return RecordSRC;
        }
        public DateTime GetPriceDTS()
        {
            string filename = "sm2150112.FID".Replace("sm2", "").Replace("FID", "");

            DateTime PriceDTS;

            bool success = DateTime.TryParseExact(filename, "yyyyMMdd 16:00:00", CultureInfo.InvariantCulture, DateTimeStyles.None, out PriceDTS);

            if (!success)
            {
                return new DateTime();
            }
            else
            {
                return PriceDTS;
            }
        }
    }
}
