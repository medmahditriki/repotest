﻿using System.Collections.Generic;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.Azure;
using System.IO;
using System.Threading.Tasks;
using DV_3;
using MainProgram;
using System;

namespace SMandSMFIDandDC
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Connecting to Blob
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            CloudBlobContainer containerDC = blobClient.GetContainerReference("dataphile-dc");
            containerDC.CreateIfNotExists();
            CloudBlobContainer containerSM = blobClient.GetContainerReference("dataphile-sm");
            containerSM.CreateIfNotExists();
            CloudBlobContainer containerSMFID = blobClient.GetContainerReference("dataphile-sm2");
            containerSMFID.CreateIfNotExists();

            #endregion






           CloudBlockBlob blockBlobDC1 = containerDC.GetBlockBlobReference("dc170308.all.JIT");
            MemoryStream memoryStream1;
            using (memoryStream1 = new MemoryStream())
            { blockBlobDC1.DownloadToStream(memoryStream1); }

            

           





            CloudBlockBlob SMblockBlob1 = containerSM.GetBlockBlobReference("sm170308");
            MemoryStream memoryStream2;
            using (memoryStream2 = new MemoryStream())
            { SMblockBlob1.DownloadToStream(memoryStream2); }

            

            







            CloudBlockBlob SM2blockBlob1 = containerSMFID.GetBlockBlobReference("sm2170308.JIT");
            MemoryStream memoryStream3;
            using (memoryStream3 = new MemoryStream())
            { SM2blockBlob1.DownloadToStream(memoryStream3); }

            

            

            DC.IFidelityFile DCfile = new DC.FidelityMemoryStream(memoryStream1);
            ClassLibrary1.File.IFidelityFile SMfile = new ClassLibrary1.File.FidelityMemoryStream(memoryStream2);
            ClassLibrary1.File.IFidelityFile SM2file = new ClassLibrary1.File.FidelityMemoryStream(memoryStream3);

            List<MemoryStream> MemoryList = new List<MemoryStream>();

            MemoryList.Add(memoryStream1);
            MemoryList.Add(memoryStream2);
            MemoryList.Add(memoryStream3);
            string FileNameDC = "dc170308.all.JIT";
            string FileNameSM = "sm170308";

            string FileNameSM2 = "sm2170308.JIT";

            Parallel.ForEach(MemoryList, (Memory) =>
             {
                 if (Memory == memoryStream1)
                 {
                     IDCtoDB DCtoDB = new DCtoDB();
                     DCtoDB.SendDCtoDB(DCfile, FileNameDC);

                 }
                 else if (Memory == memoryStream2)
                 {
                     ISMtoDB SMtoDB = new SMtoDB();
                     SMtoDB.SendSMtoDB(SMfile, FileNameSM);

                     ISMFIDtoDB SMFIDtoDB = new SMFIDtoDB();
                     SMFIDtoDB.SendSMFIDtoDB(SM2file, FileNameSM2);
                 }

             });
            //memoryStream1.SetLength(0);
            //memoryStream2.SetLength(0);
            //memoryStream3.SetLength(0);
            memoryStream1.Flush() ;
            memoryStream2.Flush();
            memoryStream3.Flush();


           // GC.Collect();
            FileNameDC = "dc170309.all.JIT";
            FileNameSM = "sm170309";
            FileNameSM2 = "sm2170309.JIT";
            CloudBlockBlob blockBlobDC2 = containerDC.GetBlockBlobReference(FileNameDC);
            using (memoryStream1 = new MemoryStream())
            { blockBlobDC2.DownloadToStream(memoryStream1); }

            CloudBlockBlob SMblockBlob2 = containerSM.GetBlockBlobReference(FileNameSM);
            using (memoryStream2 = new MemoryStream())
            { SMblockBlob2.DownloadToStream(memoryStream2); }

            CloudBlockBlob SM2blockBlob2 = containerSMFID.GetBlockBlobReference(FileNameSM2);
            using (memoryStream3 = new MemoryStream())
            { SM2blockBlob2.DownloadToStream(memoryStream3); }
            DCfile = new DC.FidelityMemoryStream(memoryStream1);
            SMfile = new ClassLibrary1.File.FidelityMemoryStream(memoryStream2);
            SM2file = new ClassLibrary1.File.FidelityMemoryStream(memoryStream3);
            Parallel.ForEach(MemoryList, (Memory) =>
            {
                if (Memory == memoryStream1)
                {
                    IDCtoDB DCtoDB = new DCtoDB();
                    DCtoDB.SendDCtoDB(DCfile, FileNameDC);

                }
                else if (Memory == memoryStream2)
                {
                    ISMtoDB SMtoDB = new SMtoDB();
                    SMtoDB.SendSMtoDB(SMfile, FileNameSM);

                    ISMFIDtoDB SMFIDtoDB = new SMFIDtoDB();
                    SMFIDtoDB.SendSMFIDtoDB(SM2file, FileNameSM2);
                }

            });
            //memoryStream1.SetLength(0);
            //memoryStream2.SetLength(0);
            //memoryStream3.SetLength(0);
           // GC.Collect();
            memoryStream1.Flush();
            memoryStream2.Flush();
            memoryStream3.Flush();
            FileNameDC = "dc170310.all.JIT";
            FileNameSM = "sm170310";
            FileNameSM2 = "sm2170310.JIT";
            CloudBlockBlob blockBlobDC3 = containerDC.GetBlockBlobReference(FileNameDC);
            using (memoryStream1 = new MemoryStream())
            { blockBlobDC3.DownloadToStream(memoryStream1); }

            CloudBlockBlob SMblockBlob3 = containerSM.GetBlockBlobReference(FileNameSM);
            using (memoryStream2 = new MemoryStream())
            { SMblockBlob3.DownloadToStream(memoryStream2); }

            CloudBlockBlob SM2blockBlob3 = containerSMFID.GetBlockBlobReference(FileNameSM2);
            using (memoryStream3 = new MemoryStream())
            { SM2blockBlob3.DownloadToStream(memoryStream3); }
             DCfile = new DC.FidelityMemoryStream(memoryStream1);
             SMfile = new ClassLibrary1.File.FidelityMemoryStream(memoryStream2);
             SM2file = new ClassLibrary1.File.FidelityMemoryStream(memoryStream3);
            Parallel.ForEach(MemoryList, (Memory) =>
            {
                if (Memory == memoryStream1)
                {
                    IDCtoDB DCtoDB = new DCtoDB();
                    DCtoDB.SendDCtoDB(DCfile, FileNameDC);

                }
                else if (Memory == memoryStream2)
                {
                    ISMtoDB SMtoDB = new SMtoDB();
                    SMtoDB.SendSMtoDB(SMfile, FileNameSM);

                    ISMFIDtoDB SMFIDtoDB = new SMFIDtoDB();
                    SMFIDtoDB.SendSMFIDtoDB(SM2file, FileNameSM2);
                }

            });
            //memoryStream1.SetLength(0);
            //memoryStream2.SetLength(0);
            //memoryStream3.SetLength(0);
        }
    }
}








